﻿<!DOCTYPE html>
<html lang="es">
<head>
<title>www.proyecto.com</title>
<meta charset="utf-8" />
<meta name="description" content="">
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-grid.css" />
<link rel="stylesheet" href="css/bootstrap-grid.min.css" />
<link rel="stylesheet" href="css/bootstrap-reboot.css" />
<link rel="stylesheet" href="css/bootstrap-reboot.min.css" />
<link rel="shortcut icon" href="/favicon.ico" />
</head>
		<body>
        <div class="container">
  			<div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
    		<div class="col"></div>
    		<div class="col"></div>
    		<div class="col"></div>
    		<div class="col"><a class="btn btn-primary" href="mostrarEmpleados.php" target="Form" role="button">REGRESAR</a></div>
		  </div>
			<?php
            
            //capturar el codigo a modificar
	        $idEmpleado=$_REQUEST['idEmpleado'];
            //cargar la conexion y octener la conexion activa $misql
            include('include/config.inc');
            $conexion = mysqli_connect($servidor,$usuario,$contrasena,$basededatos);
            mysqli_set_charset($conexion,"utf8");
        
            $query="call SELECTEMPLEADOPorID('$idEmpleado');";
            $consulta=$conexion->query($query);
            $row=$consulta->fetch_assoc(); 
        
            mysqli_close($conexion);	
               
			
			?>

                <form name="SelectEmpleado" action="updateEmpleados.php" method="post" >
                    <div class="form-row">
                        <div class="form-group col-md-6">
                        <label for="inputID">idEmpleado</label>
                        <input type="text" class="form-control" name="txtID" value="<?php echo $row['idEmpleado'];?>" style='visibility:hidde'>
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputNombre">NOMBRES</label>
                        <input type="text" class="form-control" name="txtNombre" value="<?php echo $row['nombres'];?>">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputApellido">APELLIDOS</label>
                        <input type="text" class="form-control" name="txtApellido" value="<?php echo $row['apellidos'];?>">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputDui">DUI</label>
                        <input type="text" class="form-control" name="txtDUI" value="<?php echo $row['dui'];?>">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputNit">NIT</label>
                        <input type="text" class="form-control" name="txtNIT" value="<?php echo $row['nit'];?>">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputEstado">ESTADO</label>
                        <input type="text" class="form-control" name="txtEstado" value="<?php echo $row['estado'];?>">
                        </div>
                    </div>
                    <div class='form-row'>
                        <div class="form-group col-md-6">
                        <label for="inputEmpresa">ID EMPRESA</label>
                        <input type="text" class="form-control" name="txtEmpresa" value="<?php echo $row['Empresas_idEmpresa'];?>">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputRol">ID ROL</label>
                        <input type="text" class="form-control" name="txtRol" value="<?php echo $row['Roles_idRol'];?>">
                        </div>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                </div>
                </form>
                

        
			
		<br><br><br>
		</body>
</html>