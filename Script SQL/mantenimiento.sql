-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-08-2020 a las 08:53:10
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mantenimiento`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETEEMPLEADO` (IN `par_idEmpleado` INT)  delete from  empleados where idEmpleado = par_idEmpleado$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETEEMPPLEADO` (IN `par_idEmpleado` INT)  delete from  empleados where idEmpleado = par_idEmpleado$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETEEMPRESA` (IN `par_idEmpresa` INT)  delete from  empresas where idEmpresa = par_idEmpresa$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETEROL` (IN `par_idRol` INT)  delete from  roles where idRol = par_idRol$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `INSERTEMPLEADO` (IN `par_nombre` VARCHAR(45), IN `par_apellido` VARCHAR(45), IN `par_dui` VARCHAR(10), IN `par_nit` VARCHAR(17), IN `par_estado` VARCHAR(15), IN `par_empresa` INT, IN `par_rol` INT)  insert into empleados (nombres,apellidos,dui,nit,estado,Empresas_idEmpresa,Roles_idRol)
values (par_nombre,par_apellido,par_dui,par_nit,par_estado,par_empresa,par_rol)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `INSERTEMPRESA` (IN `par_nombre` VARCHAR(45), IN `par_nit` VARCHAR(45), IN `par_telefono` VARCHAR(45), IN `par_direccion` VARCHAR(45), IN `par_municipio` VARCHAR(45), IN `par_departamento` VARCHAR(45))  insert into empresas (nombreEmpresa,nit,telefono,direccion,municipio,departamento)
values (par_nombre,par_nit,par_telefono,par_direccion,par_municipio,par_departamento)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `INSERTROL` (IN `par_nombre` VARCHAR(45), IN `par_descripcion` VARCHAR(150), IN `par_permisos` VARCHAR(150), IN `par_empresa` INT)  insert into roles (nombreRol,descripcionRol,permisos,Empresas_idEmpresa)
values (par_nombre,par_descripcion,par_permisos,par_empresa)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SELECTEMPLEADOPorID` (IN `par_idEmpleado` INT)  SELECT * FROM empleados
  where idEmpleado = par_idEmpleado$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SELECTEMPLEADOS` ()  SELECT * FROM empleados$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SELECTEMPRESAPorID` (IN `par_idEmpresa` INT)  SELECT * FROM empresas
  where idEmpresa = par_idEmpresa$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SELECTEMPRESAS` ()  SELECT * FROM empresas$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SELECTROL` ()  SELECT * FROM roles$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SELECTROLPorID` (IN `par_idRol` INT)  SELECT * FROM roles
  where idRol = par_idRol$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `idEmpleado` int(11) NOT NULL,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `dui` varchar(10) NOT NULL,
  `nit` varchar(17) NOT NULL,
  `estado` varchar(15) NOT NULL,
  `Empresas_idEmpresa` int(11) NOT NULL,
  `Roles_idRol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `idEmpresa` int(11) NOT NULL,
  `nombreEmpresa` varchar(45) NOT NULL,
  `nit` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `municipio` varchar(45) NOT NULL,
  `departamento` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `idRol` int(11) NOT NULL,
  `nombreRol` varchar(45) NOT NULL,
  `descripcionRol` varchar(150) NOT NULL,
  `permisos` varchar(150) NOT NULL,
  `Empresas_idEmpresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`idEmpleado`),
  ADD KEY `Empresas_idEmpresa` (`Empresas_idEmpresa`),
  ADD KEY `Roles_idRol` (`Roles_idRol`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`idEmpresa`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idRol`),
  ADD KEY `Empresas_idEmpresa` (`Empresas_idEmpresa`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `idEmpleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `idEmpresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD CONSTRAINT `empleados_ibfk_1` FOREIGN KEY (`Empresas_idEmpresa`) REFERENCES `empresas` (`idEmpresa`),
  ADD CONSTRAINT `empleados_ibfk_2` FOREIGN KEY (`Roles_idRol`) REFERENCES `roles` (`idRol`);

--
-- Filtros para la tabla `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`Empresas_idEmpresa`) REFERENCES `empresas` (`idEmpresa`),
  ADD CONSTRAINT `roles_ibfk_2` FOREIGN KEY (`Empresas_idEmpresa`) REFERENCES `empresas` (`idEmpresa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
