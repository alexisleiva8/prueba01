<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<meta name="description" content="">
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-grid.css" />
<link rel="stylesheet" href="css/bootstrap-grid.min.css" />
<link rel="stylesheet" href="css/bootstrap-reboot.css" />
<link rel="stylesheet" href="css/bootstrap-reboot.min.css" />
<link rel="shortcut icon" href="/favicon.ico" />
</head>
  
 <body >
<?php
	           //capturar el codigo a modificar
			   $idEmpresa=$_REQUEST['idEmpresa'];
			   //cargar la conexion y octener la conexion activa $misql
			   include('include/config.inc');
			   $conexion = mysqli_connect($servidor,$usuario,$contrasena,$basededatos);
			   mysqli_set_charset($conexion,"utf8");
		   
			   $query="call SELECTEMPRESAPorID('$idEmpresa');";
			   $consulta=$conexion->query($query);
			   $row=$consulta->fetch_assoc(); 
		   
			   mysqli_close($conexion);		
?>
	 <div class="container">
    <div class="row">
      <div class="col">
      </div>
      <div class="col-6">
				<form name="updateEmpresa" action="updateEmpresa.php" method="post">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                        <label for="inputID">IdEMPRESA</label>
                        <input type="text" class="form-control" name="txtID" value="<?php echo $row['idEmpresa'];?>" style='visibility:hidde'>
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputNombre">NOMBRE</label>
                        <input type="text" class="form-control" name="txtNombre" value="<?php echo $row['nombreEmpresa'];?>">
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for="inputNIT">NIT</label>
                        <input type="text" class="form-control" name="txtNIT" value="<?php echo $row['nit'];?>">
                    </div>
                    <div class='form-row'>
                        <div class="form-group col-md-6">
                        <label for="inputTelefono">TELEFONO</label>
                        <input type="text" class="form-control" name="txtTelefono" value="<?php echo $row['telefono'];?>">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputDireccion">DIRECCION</label>
                        <input type="text" class="form-control" name="txtDireccion" value="<?php echo $row['direccion'];?>">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputMunicipio">MUNICIPIO</label>
                        <input type="text" class="form-control" name="txtMunicipio" value="<?php echo $row['municipio'];?>">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputDepartammento">DEPARTAMENTO</label>
                        <input type="text" class="form-control" name="txtDepartamento" value="<?php echo $row['departamento'];?>">
						</div>
						<button type="submit" class="btn btn-primary">Actualizar</button>
                </div>
                </form>
                </div>
      <div class="col">
      </div>
    </div>

</body>

</head>
</html>