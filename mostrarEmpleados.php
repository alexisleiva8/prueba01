﻿<!DOCTYPE html>
<html lang="es">
<head>
<title>www.proyecto.com</title>
<meta http-equiv="Content-Type" content="text/html; charset=us-ascii"  charset="utf-8"> 
<meta name="description" content="">
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-grid.css" />
<link rel="stylesheet" href="css/bootstrap-grid.min.css" />
<link rel="stylesheet" href="css/bootstrap-reboot.css" />
<link rel="stylesheet" href="css/bootstrap-reboot.min.css" />
<link rel="shortcut icon" href="/favicon.ico" />
</head>
		<body>
		<div class="container">
  			<div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
    		<div class="col"></div>
    		<div class="col"></div>
    		<div class="col"></div>
    		<div class="col"><a class="btn btn-primary" href="NuevoEmpleado.html" target="Form" role="button">+NUEVO</a></div>
  		</div>
		</div>
		<?php
			include('include/config.inc');
			$conexion = mysqli_connect($servidor,$usuario,$contrasena,$basededatos);
			mysqli_set_charset($conexion,"utf8");
			

			$query="call SELECTEMPLEADOS();";

			$resultado=mysqli_query( $conexion, $query ) or die ( "No se pueden mostrar los registros");
			
			echo"<table class='table'>";
			echo"<thead class='thead-dark'>
					<tr>
					<th scope='col'>#</th>
					<th scope='col'>NOMBRES</th>
					<th scope='col'>APELLIDOS</th>
					<th scope='col'>ACCIONES</th>
					</tr>
				</thead>";
			while ($row=mysqli_fetch_array($resultado))
				{
				echo "<tbody>";
				echo "<tr>";				
				echo "<td>",$row['idEmpleado'],"</td>";
				echo "<td>",$row['nombres'],"</td>";
				echo "<td>",$row['apellidos'],"</td>";
				echo "<td>"."<a type='button' target='Form' class='btn btn-primary' href='mostrarEmpleadoID.php?idEmpleado=".$row['idEmpleado']."'>Ver</a>".
				"<a type='button' target='Form' class='btn btn-primary' href='modificarEmpleado.php?idEmpleado=".$row['idEmpleado']."'>Editar</a>".
				"<a type='button' class='btn btn-danger' href='eliminarEmpleado.php?idEmpleado=".$row['idEmpleado']."'>Eliminar</a>"."</td>";			
				echo "</tr>";
				echo "</tbody>";
				}
			echo "</table>";
			
			// cerrar conexión de base de datos
			mysqli_close( $conexion );
		?>
		</body>
</html>